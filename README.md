# SassyT

**TIFFANY THOMPSON**

Find me on [LinkedIn](https://www.linkedin.com/in/tthompsoninatl/)  and [X (Twitter)](https://twitter.com/sassytinatl) 


    The motto I try to live by: 🐂🤘🏼
        "Be the bull, not the horns"
>     


**ABOUT ME**

**I am results-driven builder, strategist, and intrapreneurial leader experienced in enterprise and startups.  Proven expertise in creating global marketing strategies, scaling programs, forging collaborative relationships with stakeholders, and driving operational excellence in high-growth constantly changing environments. As a founding member of the leadership team at Oracle for Startups, I helped accelerate the program from one location in India with four employees to a global virtual program attracting 3,500+ startups, a team of 90 people, creating $25 million in cloud revenue. I have previous success leading Marketing, Events & Partnerships at Vitrue, a SaaS-based MarTech platform, where I helped build the brand that led to an acquisition by Oracle in 2012.**

I’ve done just about everything under the sun that is marketing related.  Outside of that I’ve spent some time as a sous chef for a private chef and helped her write and launch a cookbook, worked for 2 Olympic games, I’ve helped several non-profits launch their social media efforts. 

In early 2010, I left a very good and comfortable job in the tourism and hospitality industry without knowing what I wanted to do next. Spent some time learning how to use social media…twitter was just was really ramping up.  I went to some TweetUps back when people were freaking out about meeting people online.  That is what kicked off my consulting business. People were asking me to show them how to use social media for their small businesses.  Through those connections I made a pivot into technology working at a social media SaaS based startup called Vitrue.  I didn’t know the target audience or the language or how to sell in a B2B tech environment. I immersed myself in it and was there about 18 months before the Oracle acquisition. Transitioning to a tech startup was an amazing experience that changed the trajectory of my career.   

I gravitate towards opportunities that allow me to build things and be creative.  Bonus points if it’s a newly created job that no one has ever done before.  Drop me into the deep end and I’ll figure out how to swim.

Recommendations from colleagues,employees and partners: 
Karlie Krieger Valine profile picture
Karlie Krieger ValineKarlie Krieger Valine
· 1stFirst degree connection
Chief Business Officer - Startup GrindChief Business Officer - Startup Grind
September 19, 2022, Tiffany was Karlie’s clientSeptember 19, 2022, Tiffany was Karlie’s client
I had the honor of working closely with Tiffany through a successful multi-year partnership we built between Oracle and Startup Grind. Together we created, tracked, and implemented various marketing activations around the world to connect our community with Oracle's offerings for startups. Tiffany always ensured the work we did together was authentic - making an impact on the community, while also delivering ROI. She was a dream partner to work with and I hope to do so together again very soon!



**Outside of Work:**
- ✈️ I love to travel, especially if I can live like a local and immerse myself in the culture.
- 💙💙💙💗 I am the proud "Ahnie" to 3 nephews and 1 niece that I enjoy spoiling and hanging out with because I learn so much from them.
- 👩🏻‍🔧 I tackled my own kitchen renovation project and gained valuable insights along the way. Demolishing walls and cabinets proved to be a great outlet for frustrations! Each task on the checklist required a different power tool, attachment, or accessory. (Result: I now have enough to open a small home improvement store.) Add an additional 25% of time to the deadlines that your vendors give you. Lastly, I learned that I am not cut out for manual labor and have a newfound respect for people who do it every day.

- 🏋🏽 I do Crossfit at least 4 days a week. It keeps me sane.

- 🏈⚽️🏌🏽‍♀️🎾 I love watching college football, soccer, and playing golf and tennis.

**-
What I Can Contribute:**

- Over 20 years experience encompassing almost every discipline of marketing.
- Quick-learner who loves to soak up new knowledge and skills.

- I find satisfaction in coaching and mentoring junior colleagues, and my professional history is marked by a consistent record of promoting and advancing team members."

- An innate ability to bring together disparate threads of workflows to propel a project forward
- Available to travel as needed.  
- Based in Eastern time zone, but I can work flexible hours. I have a lot of experience managing and collaborating with colleagues around the globe.  
- Self Motivated:  I've worked in a remote/home office environment since 2013.




**My CliftonStrengths Assessment results:**

**ACTIVATOR** - I possess a strong bias for action. This approach allows me to make decisions, take action, learn from the results and keep iterating. Growth comes from responding to challenges. This strength complements people with Focus, Strategic, Futuristic themes who can lend their direction and planning to your activation, thereby creating an opportunity to build consensus and get others behind the plan.  

**BELIEF**- I seek out purpose-driven and meaningful work. I am happiest when I am surrounded by creative and passionate people all driving towards the same goal. I am consistent and dependable, which makes me a trusted advisor with an ability to maintain confidentiality when necessary.

**COMMAND**-  Clarity, honesty and transparency are essential to me. I am comfortable taking charge in situations in order to make a path forward. 
 
**INDIVIDUALIZATION**- I love to know what makes people tick and what gets them out of bed in the morning. This trait serves me well when dealing with people of from different cultures and cross-functional lines of business.

**STRATEGIC -** I possess the ability to seamlessly transition between conceptualizing the overarching big picture vision and comprehending the intricate details of execution, including identifying potential obstacles and anticipating outcomes.


**Key highlights from my experience that align with your requirements:**

**Executive Engagement & Stakeholder Management**

•	Reported into Larry Ellison’s office under the leadership of his Chief of Staff.  As part of the leadership team for the startup program, I participated in quarterly business updates to Larry reporting on our business updates, results, and program roadmap. 

•	Deep experience setting agendas and leading all-hands calls for global teams to share business updates, promote remote team connectivity and collaboration.  During covid, we created a monthly book club zoom event for our team to boost team morale.  

•	Elevated our Founder,a threepeat entrepreneur, as a thought leader in the startup ecosystem through media pitches, analyst briefings,  strategic account customer meetings, and speaking engagements.  Sourced and vetted opportunities, created briefing docs, speaker guides, and presentation materials.  

**Adaptability and Flexibility- Working in a high-growth constantly changing environment**

•	Hired as 4th Marketing employee at Series C SaaS-based startup.  Grew the investments in events and partnerships from 10K to over 1 million in less than one year. 

•Pivoted the startup accelerator program from a highly selective, location-based accelerator program to a virtual program open to all startups globally requiring us to unwind physical locations and let employees go over the course of 7 months.

•Expanded our mandate to include supporting university researchers and student entrepreneurs by launching Oracle for Research and Oracle on Campus programs. 

Based in Eastern time zone, but I can work flexible hours. I have a lot of experience managing and collaborating with colleagues around the globe.  Available to travel as needed.
  
**Operational Excellence and Process Improvement**

•	Data-driven:  Constructed comprehensive reporting dashboards to meticulously track results, fine-tune campaign tactics, and deliver quarterly updates to leadership, ensuring a clear view of ROI.  

•	Worked on a cross-functional committee to create an integration between NetSuite CRM and Eloqua to improve our data and insights into customer and prospects behavior as well as launch lead scoring to allow sales to focus on the highest potential leads.

•	Detail-Oriented: Managed budget for marketing program, working cross-functionally with global ops team, and consistently  maintained a budget variance of less than 3%. 

